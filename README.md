# Challenges 15 - Asynchronous Callbacks

## Objectives:

- To learn about `Asynchronous Javascript`.
- To learn about `Asynchronous Callbacks`.
- To learn for seeking information, reading documentation and checking out topics 
  about `Asynchronous Callbacks`.


### Theory & Documentation


### Synchronous Javascript

Synchronous operations in JavaScript entails having each step of an operation waits for the previous step to execute completely. This means no matter how long a previous process takes, subsquent process won't kick off until the former is completed.

```javascript
console.log("Plant corn");
console.log("Water plant");
console.log("Add fertilizer");
```

We have the following logged in the console:

```
Plant corn
Water plant
Add fertilizer
```

JavaScript is by default Synchronous `single threaded`. Think about it like this – one thread means one hand with which to do stuff.

### Asynchronous Javascript

Asynchronous operations, on the other hand, defers operations. Any process that takes a lot of time to process is usually run alongside other synchronous operation and completes in the future (tasks are completed independently).

> The word 'asynchronous', just means 'takes some time' or 'happens in the future, not right now'.


```javascript
console.log("Plant maize");

setTimeout(function() {
  console.log("Water plant")
}, 3000);

console.log("Add fertilizer");
```

We get the following in the console:

```
Plant Maize
Add fertilizer
Water plant
```

The `setTimeout()` function makes the operation `asynchronous` by deferring plant watering to occur after 3 seconds. The whole operation doesn’t pause for 3 seconds so it can log "Water plant". Rather, the system goes ahead to apply fertilizers and then water plant after 3 seconds.

### Does taking a callback make a function asynchronous?

Every asynchronous function takes a function argument, but not every function that does so is asynchronous. It matters how the argument is used inside the function.

### Synchronous Callbacks

Argument functions can be called synchronously, simply taking a callback doesn't make a function asynchronous. There are many examples of functions that take a function argument but are not asynchronous.

Example using `forEach()` method:

```javascript
let totalSize = 0

items.forEach((item) => {
    totalSize += item.size
})

return totalSize
```

Another example using `map()` method:

```javascript
const list = ['man', 'woman', 'child']

const newList = list.map(function(val) {
  return val + " kind";
})
```

The `forEach()` and `map()` methods accept a callback function which states how each element of the array will be manipulated, so calling the `argument function` is performed as part of normal `step-by-step sequential execution` of statements that make up the `forEach` and `map` implementation.

### Asynchronous Callbacks

Asynchronous function needs to perform an asynchronous operation. It needs to incorporate the `argument callback` in handling the results of this asynchronous operation. Only this way the function becomes asynchronous.

> Some ways of introducing asynchronicity can be:

- Timer functions `setTimeout`, `setInterval`.
- Listening to network, querying a database.
- Reading or writing, generally I/O from a resource.
- Subscribing to an event.

Example using `setInterval()` function:

```javascript
setInterval(function() {
  console.log('hello!');
}, 1000);
```

`setInterval` accepts a callback function as its first parameter and also a time interval.

Usually `asynchronous callbacks` are used when doing I/O, e.g. downloading things, reading files, talking to databases, etc.

Example using a `callback function` to download a photo:

When you call a normal function you can use its return value:

```javascript
var result = multiplyTwoNumbers(5, 10)
console.log(result)
// 50 gets printed out
```

However, functions that are `async` and use callbacks don't return anything right away.

```javascript
var photo = downloadPhoto('http://coolcats.com/cat.gif')
// photo is 'undefined'!
```

In this case the `gif` might take a very long time to download, and you don't want your program to pause ('block') while waiting for the download to finish.

Instead, you store the code that should run after the download is complete in a function, this is the `callback`. You give it to the `downloadPhoto` function and it will run your callback (e.g. 'call you back later') when the download is completed, and pass in the photo (or an error if something went wrong).

```javascript
function handlePhoto (error, photo) {
  if (error) console.error('Download error!', error)
  else console.log('Download finished', photo)
}

downloadPhoto('http://coolcats.com/cat.gif', handlePhoto)

console.log('Download started')
```

In this example three major things happen:

- First the `handlePhoto()` function is declared,
- The `downloadPhoto()` function is invoked and passed the `handlePhoto()` as its  
  callback, and finally `'Download started'` is printed out.

Note that the `handlePhoto()` is not invoked yet, it is just created and passed as a `callback` into `downloadPhoto()`. But it won't run until `downloadPhoto()` finishes doing its task, which could take a long time depending on how fast the Internet connection is.

> This example is meant to illustrate two important concepts:

1) The `handlePhoto()` callback is just a way to store some things to do at a 
   later time.
2) The order in which things happen does not read `top-to-bottom`, it jumps around 
   based on when things are completed.


### Callback Hell

Multiple functions can be created independently and used as `callback functions`. These create multi-level functions. When this function tree created becomes too large, the code becomes incomprehensible sometimes and is not easily refactored. This is known as `callback hell`.

Using `nested callbacks` method can lead to an untameable `callback-hell`.

```javascript
getData(function(a){
    getMoreData(a, function(b){
        getMoreData(b, function(c){
            getMoreData(c, function(d){
                getMoreData(d, function(e){
                    ...//more nested callbacks...
                })
            })
        })
    })
})
```

or

```javascript
function setInfo(name) {
    address(a, function(myAddress) {
        officeAddress(b, function(myOfficeAddress) {
            telephoneNumber(c, function(myTelephoneNumber) {
                nextOfKin(d, function(myNextOfKin) {
                    console.log('done')
                    ...//more nested callbacks...
                })
            })
        })
    })
}
```

As you can see, this can really get out of hand. Throw in some `if` statements, `for` loops, `function` calls, or `comments` and you'll have some very hard-to-read code. 


### Some best practices to avoid `callback hell`

- Don't nest functions. Give them names and place them at the top level of your 
  program.
- The most important aspect of avoiding `callback hell` is moving functions out of 
  the way so that the programs flow can be more easily understood without newcomers having to wade through all the detail of the functions to get to the meat of what the program is trying to do.
- Callback functions are useful for short asynchronous operations. When working 
  with large sets, this is not considered best practice.


## Challenges 15 (Instructions) – Solving Problems 

### Nested Callbacks

Steps
- Clone repo or download.

Deriverables
- Upload the github/gitlab repository url to Trello.

Final result

Our client has decided that it is important to use `best practices` to solve the problem and he has given us some piece of code with `nested callback functions`. Also, our client has provided with an output example, so your work should evaluate it and reach it if you have written the functions correctly.

> You must use `best practices` and `callbacks`. 


<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

> References:

1. [Asynchronous Javascript](https://www.freecodecamp.org/news/javascript-async-await-tutorial-learn-callbacks-promises-async-await-by-making-icecream/)

2. [Asynchronous Callbacks](https://www.better.dev/callbacks-promises-and-async)

3. [Does taking a Callback make a Function Asynchronous](https://bytearcher.com/articles/does-taking-a-callback-make-a-function-asynchronous/)

4. [Callback Hell](http://callbackhell.com/)

5. [Best Practices Avoiding callback Hell](https://blog.risingstack.com/node-js-async-best-practices-avoiding-callback-hell-node-js-at-scale/)


6. [Avoiding Callback Hell in NodeJS](https://stackabuse.com/avoiding-callback-hell-in-node-js/)
