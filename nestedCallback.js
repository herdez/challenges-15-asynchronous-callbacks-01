// Nested Callback Functions

/*

The software team of 'NestedCall Ltd' has some problems with asynchronous javascript.
They have given us a piece of code with nested callback functions:


// fastFunction()
function fastFunction (done) {
  setTimeout(function () {
    done()
  }, 1000)
}

// slowFunction()
function slowFunction (done) {
  setTimeout(function () {
    done()
  }, 3000)
}

// runSequentially()
function runSequentially (callback) {
  fastFunction((err, data) => {
    if (err) return callback(err)
    console.log(data)   // results of a
  
    slowFunction((err, data) => {
      if (err) return callback(err)
      console.log(data) // results of b
  
      // here you can continue running more nested callbacks...
    })
  })
}

// driver code
runSequentially((err) => console.log(err))

*/

/*** 
 * We have the challenge to refactor it and follow best practices to avoid 
 * 'callback hell' and finally reaching this output example in terminal:
 *
 * ```
 * Results of a
 * Error in slowFunction
 * ``` 
 * 
 */
 

//+++ YOUR CODE GOES HERE



//fastFunction()



//slowFunction()



// runSequentially()




// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

runSequentially((err) => console.log(err))
 
console.log('*~*~*~*~*~ TEST-1 *~*~*~*~*~') 

// Output Example

/*

*~*~*~*~*~ TEST-1 *~*~*~*~*~
"Results of a"
"Error in slowFunction" 

*/

//console.log('*~*~*~*~*~ TEST-2 *~*~*~*~*~')

// Output Example

/*

*~*~*~*~*~ TEST-2 *~*~*~*~*~
Error in fastFunction

*/










/*
> References:

1. Adapted from Best Practices Avoiding callback Hell: https://blog.risingstack.com/node-js-async-best-practices-avoiding-callback-hell-node-js-at-scale/

*/